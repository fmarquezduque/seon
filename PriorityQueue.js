class Node {
    constructor(value, priority) {
        this.value = value;
        this.priority = priority;
        this.next = null;
    }
}
  
class PriorityQueue {

    constructor() {
        this.first = null;
        this.last = null;
        this.length = 0;
    }

    peek() {
        return this.first;
    }

    enqueue(value, priority) {

        const newNode = new Node(value, priority);
       
        if (this.length === 0 || priority > this.first.priority) {
            newNode.next = this.first;
            this.first = newNode;
        } else {
            let current = this.first;
            while (current.next !== null && priority <= current.next.priority) {
                current = current.next;
            }
            newNode.next = current.next;
            current.next = newNode;
        }

        this.length++;
        return this;
    }

    dequeue() {
        if (!this.first) {
            return null;
        }
        if (this.first === this.last) {
            this.last = null;
        }
        this.first = this.first.next;
        this.length--;

        return this;
    }
}
