class Node {
  constructor(value) {
    this.value = value;
    this.next = null;
    this.prev = null;
  }
}
  
class DoublyLinkedListCircular {

  constructor(value) {

    const newNode = new Node(value);

    newNode.next = newNode;
    newNode.prev = newNode;

    this.head = newNode;
    this.tail = newNode;

    this.length = 1;
  }

  append(value) {
    
    const newNode = new Node(value);

    newNode.prev = this.tail;
    this.tail.next = newNode;
    newNode.next = this.head;
    this.head.prev = newNode;

    this.tail = newNode;
    this.length++;

    return this;
  }

  delete(value){

    let currentNode = this.head;

    while (currentNode){

      if (currentNode.value === value) {

        currentNode.prev.next = currentNode.next;
        currentNode.next.prev = currentNode.prev;

        if (currentNode === this.head) {
          this.head = currentNode.next;
        }

        if (currentNode === this.tail) {
          this.tail = currentNode.prev;
        }

        this.length--;

        if (this.length === 0) {
          this.head = null;
          this.tail = null;
        }

        break;
      }

      currentNode = currentNode.next;

      if (currentNode === this.head) {
        break;
      }
    }

    return this;
  }
}
  