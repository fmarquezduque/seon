class WGraph {    
  constructor() {
    this.nodes = 0;
    this.adjacentList = {};
  }
  
  addVertex(node) {
    this.adjacentList[node] = [];
    this.nodes++;
  }

  addEdge(node1, node2, weight) {
    this.adjacentList[node1].push({node: node2, weight: weight});
    this.adjacentList[node2].push({node: node1, weight: weight});
  }
}

  