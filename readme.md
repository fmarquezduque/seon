## SEON

Seon es la recopilación de algunas estructuras de datos vistas en el cursod de Platzi ( https://platzi.com/cursos/estructuras-datos/ ) con modificaciones

### DoublyLinkedListCircular

Listas doblemente enlazadas circulares, para este caso lo que se hizo es que el primer y último nodo se encuentren conectados en doble vía.

### PriorityQueue

Cola con prioridad, acá se agrego una nueva propiedad para definir el orden en que se encolan los elementos

### PriorityStack

Pila con prioridad, acá se agrego una nueva propiedad para definir el orden en que se apilan los elementos

### WGraph

Grafo ponderado, acá se agrego un valor a la relación entre los nodos, es decir, un peso en las aristas